import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LevelmeterDataComponent } from './levelmeter-data.component';

describe('LevelmeterDataComponent', () => {
  let component: LevelmeterDataComponent;
  let fixture: ComponentFixture<LevelmeterDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LevelmeterDataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LevelmeterDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
