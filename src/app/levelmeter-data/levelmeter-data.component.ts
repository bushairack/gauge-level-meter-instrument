import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-levelmeter-data',
  templateUrl: './levelmeter-data.component.html',
  styleUrls: ['./levelmeter-data.component.scss'],
})
export class LevelmeterDataComponent implements OnInit {
  @Input() details: any;
  @Input() value: number;
  public fillHeight = 50;
  public warningBarPosition;
  public alarmBarPosition;
  public warningMaxBarPosition;
  public alarmMaxBarPosition;

  constructor() {}

  ngOnInit(): void {}
  ngOnChanges() {
    this.alarmMaxBarPosition = this.calculation(this.details.alarm[1]);
    this.fillHeight = this.calculation(this.value);
    this.warningBarPosition = this.calculation(this.details.warnings[0]);
    this.warningMaxBarPosition = this.calculation(this.details.warnings[1]);
    this.alarmBarPosition = this.calculation(this.details.alarm[0]);
  }
  calculation(data: any): number {
    const ratio = 100 / (this.details.maxValue - this.details.minValue);
    return data * ratio - this.details.minValue * ratio;
  }
}
