import { Component, Input, OnInit } from '@angular/core';
import { interval } from 'rxjs';

@Component({
  selector: 'app-level-meter',
  templateUrl: './level-meter.component.html',
  styleUrls: ['./level-meter.component.scss'],
})
export class LevelMeterComponent implements OnInit {
  public value;
  dummyData = [
    {
      minValue: -9.6,
      maxValue: 65.5,
      unit: '°C',
      alarm: [-8.0, 60.0],
      warnings: [0.0, 50.0],
    },
  ];

  constructor() {}

  ngOnInit(): void {
    const period = interval(2000);
    period.subscribe((value) => {
      this.getRandomInt();
      console.log(value);
    });
  }
  getRandomInt(
    min = this.dummyData[0].minValue,
    max = this.dummyData[0].maxValue
  ): void {
    this.value = Math.round(Math.random() * (max - min) + min);
    // console.log('value', this.value);
  }
}
