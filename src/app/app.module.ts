import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LevelMeterComponent } from './level-meter/level-meter.component';
import { LevelmeterDataComponent } from './levelmeter-data/levelmeter-data.component';

@NgModule({
  declarations: [AppComponent, LevelMeterComponent, LevelmeterDataComponent],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
