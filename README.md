## Level Meter Instrument
This app is created in Angular. This is an ideal instrument for the automatic measurement of temperature. This instrument measure level within a specified range and fill a color the exact amount of substance in a certain place,
while this instrument only indicate whether the temperature is above or below the warning values. Generally the latter detect levels that are excessively high or low the alarm values.

For this project contains two components: level-meter and levelmeter-data.
All the datas are itrated from one component to another. Deppends on the comming value the instrument fill the color and check the value is above or below the warning value or alarm value. When this condition is true it will display the message.

Sample Output:

![alt text](levelMeterInstrument.png)
